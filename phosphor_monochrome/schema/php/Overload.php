<?php

if (! (isset($imgfilter) && $imgfilter))
        $imgfilter = 'brightness(0.5) sepia(1) hue-rotate(200deg) saturate(1) contrast(1.5) brightness(3) blur(0.2px)';
