<?php

if (! (isset($imgfilter) && $imgfilter))
        $imgfilter = 'brightness(0.5) sepia(1) hue-rotate(355deg) saturate(3) contrast(1.2) brightness(1.8) blur(0.2px)';
